package rentcar.entities.car;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import lombok.experimental.Accessors;
import rentcar.entities.BaseEntity;
import rentcar.entities.branch.Branch;

@Entity(name = "car")
@Accessors(chain = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Car extends BaseEntity {

    private String brand;
    private Long engineVolume;
    private Long mileage;

    @JsonBackReference
    @ManyToOne
    private Branch branch;
}
