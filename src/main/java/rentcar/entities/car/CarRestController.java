package rentcar.entities.car;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import rentcar.entities.branch.Branch;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/car")
@RequiredArgsConstructor
public class CarRestController {
    private final CarServiceJpa carService;
    private final CarRepositoryEM carRepositoryEM;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Long create(@RequestBody Car car) {
        return carService.create(car);
    }

    @PutMapping("/{id}")
    public void update(@PathVariable Long id, @RequestBody Car car) {
        carService.update(id, car);
    }

    @GetMapping("/{id}")
    public Car findById(@PathVariable Long id) {
        return carService.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Car " + id + " not found"));
    }

    @GetMapping
    public Collection<Car> findAll() {
        return carService.findAll();
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        carService.deleteById(id);
    }

    @GetMapping(value = "/get-jpa-query")
    public Optional<Car> findByBrand(@RequestParam String brand) {
        return carService.findByBrand(brand);
    }

    @GetMapping(value = "/get-custom-query")
    public Optional<Car> findCarByBrandQuery(@RequestParam String brand) {
        return carService.findCarByBrandQuery(brand);
    }

    @GetMapping(value = "/getCarList")
    List<Car> carList() {
        return carRepositoryEM.carList();
    }

    @GetMapping(value = "/getCarByIdHibernate/{id}")
    public Car getCarByIdHibernate(@PathVariable Long id) {
        return carRepositoryEM.getCarByIdHibernate(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Car " + id + " not found"));
    }

//    @GetMapping("/getBranchByCarBranch")
//    public List<Branch> getBranchByCarBranchId() {
//        return carRepositoryEM.getBranchByCarBranchId();
//    }

}
