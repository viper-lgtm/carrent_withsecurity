package rentcar.entities.car;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface CarRepositoryJpa extends JpaRepository<Car, Long> {

    Optional<Car> findByBrandIgnoreCase(String brand);

    @Query("FROM car c WHERE c.brand LIKE %:brand%")
    Optional<Car> findCarByBrandQuery(String brand);
}
