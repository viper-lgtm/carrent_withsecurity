package rentcar.entities.car;

import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import rentcar.entities.branch.Branch;

import java.util.List;
import java.util.Optional;

@Repository
@Transactional
@RequiredArgsConstructor
public class CarRepositoryEM {
    private final EntityManager entityManager;

    public List<Car> carList() {
        return entityManager.createQuery("FROM car", Car.class).getResultList();
    }

    public Optional<Car> getCarByIdHibernate(Long id) {
        return Optional.ofNullable(entityManager.find(Car.class, id));
    }

//    public List<Branch> getBranchByCarBranchId() {
//        return entityManager.createQuery("FROM car c JOIN c.branch WHERE c.engineVolume > 3").getResultList();
//    }
}
