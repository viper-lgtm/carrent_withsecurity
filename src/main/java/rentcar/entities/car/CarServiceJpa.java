package rentcar.entities.car;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import rentcar.entities.branch.Branch;
import rentcar.entities.branch.BranchRepository;

import java.util.*;

@Service
@RequiredArgsConstructor
@Slf4j
public class CarServiceJpa implements CarService {
    private final CarRepositoryJpa carRepositoryJpa;
    private final BranchRepository branchRepository;
    Scanner scanner = new Scanner(System.in);

    @Override
    public Long create(Car car) {
        Long carId = null;
        log.info("Creating car...");
        log.info("Looking for the branches to assign the car...");
        List<Branch> branchList = branchRepository.findAll();
        if (branchList.size() == 0) {
            System.out.println("There are no branches in the database, store branch first.");
        } else {
            log.info("Branches found in the database.");
            branchList
                    .stream()
                    .sorted(Comparator.comparing(b -> b.getId()))
                    .forEach(branch -> {
                        System.out.println("Id: " + branch.getId() + ", Branch name: " + branch.getBranchName());
                    });
            System.out.println("Write in the console branch 'Id' number where you want to store the car and press Enter.");
            long idNum = scanner.nextLong();
            log.info("Saving car...");
            for (Branch b : branchList) {
                if (b.getId() == idNum) {
                    Car car1 = car.setBranch(b);
                    Car savedCar = carRepositoryJpa.save(car1);
                    if (!Objects.isNull(savedCar)) {
                        carId = savedCar.getId();
                        System.out.println("Your car was successfully stored into database under ID " + carId);
                    } else {
                        System.out.println("Car save error");
                    }
                }
            }
        }
        return carId;
    }

    @Override
    public void update(Long id, Car car) {
        log.info("Updating car");
        carRepositoryJpa.findById(id)
                .ifPresentOrElse(car1 -> {
                            car1.setBrand(car.getBrand());
                            car1.setEngineVolume(car.getEngineVolume());
                            car1.setMileage(car.getMileage());
                            Car savedCar = carRepositoryJpa.save(car1);
                            if (!Objects.isNull(savedCar)) {
                                System.out.println("Your car was successfully updated!");
                            }
                        },
                        () -> System.out.println("Didn't find any car.")
                );
    }

    @Override
    public Optional<Car> findById(Long id) {
        return carRepositoryJpa.findById(id);
    }

    @Override
    public Collection<Car> findAll() {
        return carRepositoryJpa.findAll();
    }

    @Override
    public void deleteById(Long id) {
        carRepositoryJpa.deleteById(id);
    }

    @Override
    public Optional<Car> findByBrand(String brand) {
        return carRepositoryJpa.findByBrandIgnoreCase(brand);
    }

    @Override
    public Optional<Car> findCarByBrandQuery(String brand) {
        return carRepositoryJpa.findCarByBrandQuery(brand);
    }
}
