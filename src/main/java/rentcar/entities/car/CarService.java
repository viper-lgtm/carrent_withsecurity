package rentcar.entities.car;

import java.util.Collection;
import java.util.Optional;

public interface CarService {
    Long create(Car car);
//    Long create(Long branchId, Car car);
    void update(Long id, Car car);
    Optional<Car> findById(Long id);
    Collection<Car> findAll();
    void deleteById(Long id);
    Optional<Car> findByBrand(String brand);
    Optional<Car> findCarByBrandQuery(String brand);
}
