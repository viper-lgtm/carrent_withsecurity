package rentcar.entities.branch;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Collection;

@RestController
@RequestMapping("/branch")
@RequiredArgsConstructor
public class BranchRestController {
    private final BranchServiceJpa branchService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Long create(@Valid @RequestBody Branch branch) {
        return branchService.create(branch);
    }

    @PutMapping("/update/{id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void update(@PathVariable Long id, @RequestBody Branch branch) {
        branchService.update(id, branch);
    }

    @GetMapping(value = "/getbranch/{id}")
    public Branch findById(@PathVariable Long id) {
        return branchService.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Branch " + id + " not found"));
    }

    @GetMapping
    public Collection<Branch> findAll() {
        return branchService.findAll();
    }

    @GetMapping(value = "/{branchlist}")
    public void listAll(@PathVariable String branchlist) {
        branchService.listAll();
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable Long id) {
        branchService.deleteById(id);
    }

//    @ExceptionHandler(NullPointerException.class)
//    @ResponseStatus(HttpStatus.BAD_REQUEST)
//    public Object handle(NullPointerException e) {
//        return Map.of("error", "Nullpointer exception",
//                "message", e.getMessage()
//        );
//    }
}
