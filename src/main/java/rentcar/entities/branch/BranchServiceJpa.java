package rentcar.entities.branch;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@RequiredArgsConstructor
@Slf4j
public class BranchServiceJpa implements BranchService {
    private final BranchRepository branchRepository;

    @Override
    public Long create(Branch branch) {
        log.info("Saving branch...");
        Long branchId = null;
        Branch savedBranch = branchRepository.save(branch);
        if (!Objects.isNull(savedBranch)) {
            branchId = savedBranch.getId();
            System.out.println("Branch stored into database under ID " + branchId);
        } else {
            System.out.println("Branch save error");
        }
        return branchId;
    }

    @Override
    public void update(Long id, Branch branch) {
        log.info("Updating branch...");
        branchRepository.findById(id)
                .ifPresentOrElse(branch1 -> {
                    branch1.setBranchName(branch.getBranchName());
                    branch1.setCapacity(branch.getCapacity());
                    Branch saveBranch = branchRepository.save(branch1);
                    if (!Objects.isNull(saveBranch)) {
                        System.out.println("Your branch was successfully updated!");
                    }
                }, () -> System.out.println("Didn't find any branch."));
    }

    @Override
    public Optional<Branch> findById(Long id) {
        return branchRepository.findById(id);
    }

    @Override
    public Collection<Branch> findAll() {
        return branchRepository.findAll();
    }

    @Override
    public void listAll() {
        List<Branch> branchList = branchRepository.findAll();
        branchList.stream()
                .sorted(Comparator.comparing(b -> b.getId()))
                .forEach(System.out::println);
    }

    @Override
    public void deleteById(Long id) {
        branchRepository.deleteById(id);
    }


}
