package rentcar.entities.branch;

import rentcar.entities.branch.Branch;

import java.util.Collection;
import java.util.Optional;

public interface BranchService {
    Long create(Branch branch);
    void update(Long id, Branch branch);
    Optional<Branch> findById(Long id);
    Collection<Branch> findAll();
    void listAll();
    void deleteById(Long id);
}
