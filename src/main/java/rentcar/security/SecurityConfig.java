package rentcar.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;
import rentcar.user.UserDetailServiceJpa;

@Configuration
@EnableWebSecurity
public class SecurityConfig {

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http, UserDetailServiceJpa userDetailServiceJpa) throws Exception {
        http.authorizeHttpRequests()
                .requestMatchers("/branch**").hasAuthority("admin")
//                .requestMatchers("/branch**").authenticated()
                .requestMatchers("/car**").permitAll()
                .and()
                .httpBasic()
                .and()
                .csrf().disable()
                .headers().frameOptions().disable()
                .and()
                .formLogin()
                .and()
                .logout()
                .and()
//                .cors().disable()
                .userDetailsService(userDetailServiceJpa);
        return http.build();
    }
//    @Bean
//    public SecurityFilterChain securityFilterChain(HttpSecurity http, PasswordEncoder encoder) throws Exception {
//        http.authorizeHttpRequests()
//                .requestMatchers("/branch/**").authenticated()
//                .requestMatchers("/car**").permitAll()
//                .and()
//                .httpBasic()
//                .and()
//                .userDetailsService(
//                        new InMemoryUserDetailsManager(
//                                User.builder()
//                                        .username("marty")
//                                        .password(encoder.encode("1234"))
////                                        .password("$2a$12$V7mfQsZqG8/1J0nrGVAMSubHqkLmP7abNfsfRv6I/1U.JDuDaclUi")
//                                        .roles("admin")
//                                        .build(),
//                                User.builder()
//                                        .username("user2")
//                                        .password(encoder.encode("5678"))
////                                        .password("$2a$12$V7mfQsZqG8/1J0nrGVAMSubHqkLmP7abNfsfRv6I/1U.JDuDaclUi")
//                                        .roles("admin")
//                                        .build()
//                        )
//                );
//        return http.build();
//    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
