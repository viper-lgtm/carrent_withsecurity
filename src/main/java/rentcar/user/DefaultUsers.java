package rentcar.user;


import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class DefaultUsers implements CommandLineRunner {

    private final UserRepository repository;
    private final PasswordEncoder encoder;

    @Override
    public void run(String... args) throws Exception {
        repository.save(
                new User()
                        .setUsername("marty")
                        .setPassword(encoder.encode("1234"))
                        .setRole("admin")
        );
    }
}
